# rpi4-ethernet-gadget

This is all the configuration for using a raspberry pi 4 as an ethernet gadget.

Inspired by https://www.hardill.me.uk/wordpress/2019/11/02/pi4-usb-c-gadget/

Requirements:
 * [Homebrew](https://brew.sh)
 * Ansible
 * sshpass

This uses ansible extensively.  `brew install ansible sshpass` or `apt install ansible sshpass` before you start.

To create a USB Ethernet Gadget Pi:
* Download raspbian (either one from https://www.raspberrypi.org/downloads/raspbian/)
* Use [Balena Etcher](https://github.com/balena-io/etcher) or the Chromebook Recovery Tool to write it to an SD Card.
* Get this repository: `git clone https://gitlab.com/randall.mason/pi4-ethernet-gadget.git`
* Create a file named `ssh` on the `boot` volume of the RaspberryPi's SD Card (Should show up in Finder or in any file explorer as a drive)
  * There is an empty file in this directory for easy copy-pasta!
* Create a wifi settings file (`wpa_supplicant.conf`) in the same place based off of the file in this directory if you don't have a wired network.
  * Enter your Country Code (Like `US`)
  * Enter your SSID (Network Name)
  * Enter your PSK (The password for your network)
* Eject that drive and put it in a Raspberry Pi 4 hooked to the network (either wired or, if you edited the `wpa_supplicant.conf`, wireless).  You may power it with your computer's USB or the wall plug at this point.
* wait for it to boot up (just wait a minute or two, or, plug it into a monitor and wait for it to print `raspberrypi login:` on the screen)
* Edit the variables in `group_vars/all` in the place where you cloned this repo or set them from the command line:
* run `ansible-playbook site.yaml -l "bootstrap" -vv -e "wifi_country=US wifi_ssid=FamilyWiFi wifi_psk='Bla Bla Bla' local_username=$USER gpu_mem=16"`
   * Depending on your usage, you may want more or less RAM used for the GPU.  The default is 512 MB, the above line overrides it to 16M.  If you are doing graphics intensive applications, set it higher, if you are doing server applications, set it to 16M, the lowest it can be set.
